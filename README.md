# Repo Reset

A simple tool for resetting the content of a RDF4J repository.

## Requirements

- Java 11 or later
- Target server - RDF4J 3.x
  - Note that this application does not work with RDF4J 4.x
- Apache Maven 3.x

## Configuration

| Option | Description                                                              | Required |
|:-------|:-------------------------------------------------------------------------|:--------:|
| `i`    | Path to the input file containing repository data backup.                |  `true`  |
| `r`    | Target repository URL                                                    |  `true`  |
| `m`    | Determines how the reset will be applied. See below for possible values. | `false`  |
| `u`    | Repository username                                                      | `false`  |
| `p`    | Repository password                                                      | `false`  |

### Reset mode
The following modes can be used for the reset:
- `clear` - clears the repository before inserting the backup data. This is the default option.
- `add` - adds the backup data to the existing content of the repository.
- `replace-graphs` - clears existing named graphs in the repository and inserts those in the backup data.

## Use

The simplest way is to use Maven to build and execute the application. For example:

`mvn package exec:java -Dexec.args="-i data.rdf -r http://localhost:8080/rdf4j-server/repositories/test -m replace-graphs"`

Note that it is first necessary to build the application (`package`).
