package cz.cvut.kbss.repo.reset;

public class Constants {

    public static final ResetMode DEFAULT_RESET_MODE = ResetMode.CLEAR;

    private Constants() {
        throw new AssertionError();
    }
}
