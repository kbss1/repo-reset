package cz.cvut.kbss.repo.reset;

import cz.cvut.kbss.repo.reset.exception.RepositoryResetException;
import joptsimple.OptionSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (args.length == 0) {
            printHelp();
            return;
        }
        final OptionSet os = ParamOptionParser.INSTANCE.parse(args);
        final CliParams input = new CliParams(os);

        try {
            new RepositoryReset(input).resetRepositoryContents();
        } catch (RepositoryResetException e) {
            LOG.error("Unable to reset repository content.", e);
        }
    }

    private static void printHelp() {
        final PrintStream os = System.out;

        try {
            ParamOptionParser.INSTANCE.printHelpOn(os);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
