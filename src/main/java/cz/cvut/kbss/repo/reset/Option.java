package cz.cvut.kbss.repo.reset;

public enum Option {

    INPUT("i", "Input file containing backup data to use"),
    TARGET_REPO("r", "Target repository URL"),
    MODE("m", "Reset mode"),
    REPO_USERNAME("u", "(Optional) repository username"),
    REPO_PASSWORD("p", "(Optional) repository password");

    public final String arg;
    final String description;

    Option(String arg, String description) {
        this.arg = arg;
        this.description = description;
    }
}
