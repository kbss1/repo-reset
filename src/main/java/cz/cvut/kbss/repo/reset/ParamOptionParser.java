/**
 * Copyright (C) 2022 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.repo.reset;

import joptsimple.OptionParser;
import joptsimple.OptionSpecBuilder;

/**
 * Option parser capable of accepting our params
 */
public class ParamOptionParser extends OptionParser {

    public static final ParamOptionParser INSTANCE = build();

    public OptionSpecBuilder accepts(final Option p) {
        return accepts(p.arg, p.description);
    }

    public static ParamOptionParser build() {
        final ParamOptionParser p = new ParamOptionParser();
        p.accepts(Option.INPUT).withRequiredArg().ofType(String.class);
        p.accepts(Option.TARGET_REPO).withRequiredArg().ofType(String.class);
        p.accepts(Option.REPO_USERNAME).withOptionalArg().ofType(String.class);
        p.accepts(Option.REPO_PASSWORD).withOptionalArg().ofType(String.class);
        p.accepts(Option.MODE).withRequiredArg().ofType(String.class).defaultsTo(Constants.DEFAULT_RESET_MODE.getValue());
        return p;
    }
}
