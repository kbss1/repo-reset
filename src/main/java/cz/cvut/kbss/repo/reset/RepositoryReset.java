package cz.cvut.kbss.repo.reset;

import cz.cvut.kbss.repo.reset.exception.RepositoryResetException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryProvider;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Resets repository contents.
 */
public class RepositoryReset {

    public static final Logger LOG = LoggerFactory.getLogger(RepositoryReset.class);

    private final CliParams params;

    public RepositoryReset(CliParams params) {
        this.params = params;
    }

    public void resetRepositoryContents() {
        LOG.info("Beginning repository reset.");
        final Repository repo = connect();
        try {
            final ResetMode mode = ResetMode.fromParam(params.valueOf(Option.MODE.arg));
            switch (mode) {
                case REPLACE_GRAPHS:
                    replaceGraphs(repo);
                    break;
                case CLEAR:
                    clearRepository(repo);
                case ADD:    // Intentional fall-through
                    insertData(repo);
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported reset mode " + mode);
            }
            LOG.info("Repository reset finished.");
        } finally {
            repo.shutDown();
        }
    }

    private Repository connect() {
        final String repoUrl = params.valueOf(Option.TARGET_REPO.arg).toString();
        LOG.debug("Connecting to repository '{}'.", repoUrl);
        final String repoId = RepositoryProvider.getRepositoryIdOfRepository(repoUrl);
        final RepositoryManager mngr = RepositoryProvider.getRepositoryManagerOfRepository(repoUrl);
        if (params.has(Option.REPO_USERNAME.arg) && mngr instanceof RemoteRepositoryManager) {
            final String username = params.valueOf(Option.REPO_USERNAME.arg).toString();
            final String password = params.valueOf(Option.REPO_PASSWORD.arg).toString();
            LOG.trace("Using credentials '{}'/***** to connect to a remote repository.", username);
            ((RemoteRepositoryManager) mngr).setUsernameAndPassword(username, password);
        }
        return mngr.getRepository(repoId);
    }

    private void replaceGraphs(Repository repo) {
        final Repository localRepo = loadBackup();
        try (final RepositoryConnection localConn = localRepo.getConnection()) {
            try (RepositoryConnection conn = repo.getConnection()) {
                LOG.debug("Clearing existing repository contexts.");
                conn.begin();
                conn.getContextIDs().forEach(conn::clear);
                conn.commit();

                conn.begin();
                localConn.getContextIDs().forEach(ctx -> {
                    conn.add(localConn.getStatements(null, null, null, false, ctx), ctx);
                    LOG.debug("Adding data from context <{}>.", ctx);
                });
                conn.commit();
            }
        }
        localRepo.shutDown();
    }

    private Repository loadBackup() {
        final File input = new File(params.valueOf(Option.INPUT.arg).toString());
        LOG.debug("Loading backup from file '{}'.", input);
        final SailRepository localRepo = new SailRepository(new MemoryStore());
        try (final RepositoryConnection localConn = localRepo.getConnection()) {
            localConn.add(input);
        } catch (IOException e) {
            throw new RepositoryResetException("Unable to read data from backup file.", e);
        }
        return localRepo;
    }

    private void clearRepository(Repository repo) {
        LOG.debug("Clearing repository.");
        try (RepositoryConnection con = repo.getConnection()) {
            con.clear();
        }
    }

    private void insertData(Repository repo) {
        try (RepositoryConnection con = repo.getConnection()) {
            final File input = new File(params.valueOf(Option.INPUT.arg).toString());
            LOG.debug("Loading backup from file '{}'.", input);
            LOG.debug("Adding data to repository.");
            con.add(input);
        } catch (IOException e) {
            throw new RepositoryResetException("Unable to insert backup data into repository.", e);
        }
    }
}
