package cz.cvut.kbss.repo.reset;

import java.util.Objects;

/**
 * Mode in which the reset should be performed.
 */
public enum ResetMode {
    /**
     * Clear repository before inserting the backup.
     */
    CLEAR("clear"),
    /**
     * Replace only named graphs.
     */
    REPLACE_GRAPHS("replace-graphs"),
    /**
     * Insert backup into the repository, without removing anything.
     */
    ADD("add");

    private final String value;

    ResetMode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ResetMode fromParam(Object param) {
        if (param == null) {
            return Constants.DEFAULT_RESET_MODE;
        }
        for (ResetMode m : values()) {
            if (Objects.equals(m.value, param)) {
                return m;
            }
        }
        throw new IllegalArgumentException("Unsupported mode value " + param);
    }
}
