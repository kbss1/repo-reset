package cz.cvut.kbss.repo.reset.exception;

public class RepositoryResetException extends RuntimeException {

    public RepositoryResetException(String message) {
        super(message);
    }

    public RepositoryResetException(String message, Throwable cause) {
        super(message, cause);
    }
}
